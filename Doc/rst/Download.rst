Download
********

The current version of *Ptools** may be downloaded using the open source version control system `git <http://git-scm.com>`_::

  git clone https://gitlab.com/revaz/Ptools.git Ptools

