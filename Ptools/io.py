# standard modules
from numpy import *
from io import IOBase

import pickle

#####################################################


def read_ascii(
        file,
        columns=None,
        lines=None,
        dtype=float,
        skipheader=False,
        cchar='#'):
    #####################################################
    """[X,Y,Z] = READ('FILE',[1,4,13],lines=[10,1000])
    Read columns 1,4 and 13 from 'FILE'  from line 10 to 1000
    into array X,Y and Z

    file is either fd or name file

    """

    def RemoveComments(l):
        if l[0] == cchar:
            return None
        else:
            return l

    def toNumList(l):
        return list(map(dtype, l))

    if not isinstance(file, IOBase):
        f = open(file, 'r')
    else:
        f = file

    # read header while there is one
    while True:
        fpos = f.tell()
        header = f.readline()
        if header[0] != cchar:
            f.seek(fpos)
            header = None
            break
        else:
            if skipheader:
                header = None
            else:
                # create dict from header
                header = str.strip(header[2:])
                elts = str.split(header)
                break

    # now, read the file content
    lines = f.readlines()

    # remove trailing
    lines = list(map(str.strip, lines))

    # remove comments
    #lines = map(RemoveComments, lines)

    # split
    lines = list(map(str.split, lines))

    # convert into float
    lines = list(map(toNumList, lines))

    # convert into array
    lines = array(list(map(array, lines)))

    # transpose
    lines = transpose(lines)

    if header is not None:
        iobs = {}
        i = 0
        for elt in elts:
            iobs[elt] = i
            i = i + 1

        vals = {}
        for key in list(iobs.keys()):
            vals[key] = lines[iobs[key]]

        return vals

    # return
    if columns is None:
        return lines
    else:
        return lines.take(axis=0, indices=columns)


#####################################################
def old_read_ascii(file, columns, lines=None, dtype=float):
    #####################################################
    """[X,Y,Z] = READ('FILE',[1,4,13],lines=[10,1000])
    Read columns 1,4 and 13 from 'FILE'  from line 10 to 1000
    into array X,Y and Z

    file is either fd or name file

    """
    try:
        import mmap
    except BaseException:
        pass

    if not isinstance(file, IOBase):
        f = open(file, 'r+')
    else:
        f = file

    f.seek(0, 2)
    fsize = f.tell()
    f.seek(0, 0)
    start = 1
    end = 0
    numcols = len(columns)

    if lines is None:
        lines = [start, end]
    elif len(lines) != 2:
        raise Exception("lines!=[start,end]")
    [start, end] = list(map(int, lines))
    start = start - 1

    nl = len(f.readlines())
    f.seek(0)
    if (end == 0) or (end > nl):
        end = nl
    numlines = end - start

    block = 65536  # chunk to read, in bytes
    data = mmap.mmap(f.fileno(), fsize)
    for i in range(start):
        junk = data.readline()

    numcols = len(columns)
    if dtype == int:
        a = zeros((numlines, numcols), 'l')
    else:
        a = zeros((numlines, numcols), 'd')

    i = 0
    line = data.readline()
    while line and (i < numlines):
        d = array(list(map(dtype, str.split(line))))
        a[i, :] = take(d, columns)
        line = data.readline()
        i = i + 1
    data.close()
    if not isinstance(file, IOBase):
        f.close()
    b = []
    for i in range(numcols):
        b.append(a[:, i])
    del a
    return b


##########################################################################
#
#  hdf5 routines
#
##########################################################################


'''
try:
  from tables import *
  is_hdf5 = True
except:
  is_hdf5 = False

def info_hdf5(file,all=False):

  if not is_hdf5:
    raise "function info_hdf5 needs tables"

  h5file = openFile(file, mode = "r")


  for elt in h5file.root._v_attrs._f_list("user"):
    val = getattr(h5file.root._v_attrs,elt)
    print "%s                   : %s"%(elt,val.__repr__())

  print
  print

  for elt in h5file.root._f_iterNodes():
    vec = elt.read()
    print "-- %s --                 "%(elt.name)
    print "len %s               : %s"%(elt.name,len(vec).__repr__())
    print "shape %s             : %s"%(elt.name,vec.shape.__repr__())

    if all:
      print "%s pos[0]          : %s"%(elt.name,vec[0].__repr__())
      print "%s pos[-1]         : %s"%(elt.name,vec[-1].__repr__())

    print




def read_hdf5(file):

  if not is_hdf5:
    raise "function read_hdf5 needs tables"

  h5file = openFile(file, mode = "r")

  data = {}

  for elt in h5file.root._f_iterNodes():
    data[elt.name] =  elt.read()

  for elt in h5file.root._v_attrs._f_list("user"):
    data[elt] =  getattr(h5file.root._v_attrs,elt)

  return data




def write_hdf5(file,data,title='hdf5'):

  if not is_hdf5:
    raise "function write_hdf5 needs tables"

  h5file = openFile(file, mode = "w", title = title)

  for elt in data.keys():

    if type(data[elt])==ndarray:
      h5file.createArray('/', elt, data[elt],"")
    else:
      setattr(h5file.root._v_attrs,elt,data[elt])


  h5file.flush()
  h5file.close()

'''


#####################################################
def write_dmp(file, data):
    #####################################################
    '''
    Write a dmp (pickle) file. In other word,
    dump the data object.

    Parameters
    ----------
    file : the path to a file
    data : a pickable python object

    Examples
    --------
    >>> x = {'a':1,'b':2}
    >>> io.write_dmp('/tmp/afile.dmp',x)
    '''
    f = open(file, 'wb')
    pickle.dump(data, f)
    f.close()


#####################################################
def read_dmp(file):
    #####################################################
    '''
    Read a dmp (pickle) file.

    Parameters
    ----------
    file : the path to a file

    Returns
    -------
    data : a python object

    Examples
    --------
    >>> x = {'a':1,'b':2}
    >>> io.write_dmp('/tmp/afile.dmp',x)
    >>> y = io.read_dmp('/tmp/afile.dmp')
    >>> y
    {'a': 1, 'b': 2}
    '''

    f = open(file, 'rb')
    data = pickle.load(f)
    f.close()
    return data


#################################
def read_params(file):
    #################################
    '''
    Read params Gadget file and return the content in
    a dictionary
    '''

    f = open(file, "rb")
    lines = f.readlines()
    f.close()

    # remove empty lines
    lines = [l for l in lines if l != b'\n']

    # remove trailing
    lines = list(map(bytes.strip, lines))

    # remove comments
    lines = [x for x in lines if len(x.decode("latin-1"))>0 and ( x.decode("latin-1")[0] != '%' or x.decode("latin-1")[0] != '#') ]
    
    # split lines
    elts = list(map(bytes.split, lines))
    

    

    # make dictionary
    params = {}
    for e in elts:
            
        name = e[0].decode("latin-1")
        try:
            params[name] = float(e[1])
        except ValueError:
            params[name] = e[1]

    return params

