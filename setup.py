#!/usr/bin/env python
'''
 @package   pNbody
 @file      setup.py
 @brief     Install pNbody
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from setuptools import setup, find_packages
from glob import glob

# DO NOT TOUCH BELOW

# scripts to install
scripts = glob("scripts/*")

# data
#data = [("rgb_tables", glob("rgb_tables/*"))]
package_files  = glob('./rgb_tables/*')



setup(
    name="Ptools",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz",
    description="""Ptools module""",
    license="GPLv3",
    version="5.0",

    packages=find_packages(),
    scripts=scripts,
    install_requires=["numpy", "scipy", "h5py",
                      "astropy", "pNbody", "matplotlib"],
    #data_files=data
    package_data={'Ptools': package_files}
)
